import sys
import os


print("============[System Info]=============")
print(sys.version)
print(sys.executable)
print(sys.platform)
print(os.getcwd())
print("======================================")
